

技术栈: vue2.x + elementUI
根据老师的项目额外增加了2个模块
1. 首页(粉丝分布)
![输入图片说明](public/readme/1.jpg)
2.echarts图表
![输入图片说明](public/readme/2.jpg)

## 1.克隆项目

git clone git@gitee.com:smart-x/toutiao-pc.git

## 2.安装依赖

npm i || cnpm i

## 3.运行项目

npm run serve

### 4.杂项说明

请注意,首页的粉丝分布是假数据,我弄着玩的
### 结束语

如果你觉得该项目对你有用的话,就点个 star 支持一下吧~

## 博客

欢迎访问我的博客[www.smartxy.cc](http://www.smartxy.cc/)
