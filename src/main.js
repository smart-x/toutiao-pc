import './assets/css/index.less'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import './permission'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/iconfont.less'
import 'element-tiptap/lib/index.css'
import { ElementTiptapPlugin } from 'element-tiptap'

import * as echarts from 'echarts'
// 浏览器兼容性css文件
import 'normalize.css/normalize.css'
import 'echarts-liquidfill'
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

// 原型注册echarts,这样在组件中使用this.$echarts就可以
Vue.prototype.$echarts = echarts
// 中央总线
Vue.prototype.$bus = new Vue()
// 安装 element-tiptap 插件
Vue.use(ElementTiptapPlugin, {
  lang: 'zh'
})
// 现在你已经在全局注册了 `el-tiptap` 组件。
Vue.use(ElementUI)
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
