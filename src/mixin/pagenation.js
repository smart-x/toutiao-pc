export default {
  data () {
    return {
      // 当前页
      currentPage: 1,
      // 每页数量
      pageSize: 3,
      // 是否禁用分页器
      ispaginationDisable: false
    }
  }
}
