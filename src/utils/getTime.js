import moment from 'moment'
/**
 * @author xiamingyu
 * @param {String} data
 * @return {Array}
 */
function getTime (data) {
  const arr = []
  const strategy = {
    day: () => {
      const today = moment().format('YYYY-MM-DD')
      arr.push(today, today)
    },
    week: () => {
      const startTime = moment(
        moment()
          .week(moment().week())
          .startOf('week')
          .valueOf()
      ).format('YYYY-MM-DD')
      const endTime = moment(
        moment()
          .week(moment().week())
          .endOf('week')
          .valueOf()
      ).format('YYYY-MM-DD')
      arr.push(startTime, endTime)
    },
    month: () => {
      const startTime = moment(
        moment()
          .month(moment().month())
          .startOf('month')
          .valueOf()
      ).format('YYYY-MM-DD')
      const endTime = moment(
        moment()
          .month(moment().month())
          .endOf('month')
          .valueOf()
      ).format('YYYY-MM-DD')
      arr.push(startTime, endTime)
    },
    year: () => {
      const startTime = moment(
        moment()
          .year(moment().year())
          .startOf('year')
          .valueOf()
      ).format('YYYY-MM-DD')
      const endTime = moment(
        moment()
          .year(moment().year())
          .endOf('year')
          .valueOf()
      ).format('YYYY-MM-DD')
      arr.push(startTime, endTime)
    }
  }
  strategy[data]()
  return arr
}
export default getTime
